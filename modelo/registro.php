<?php
require_once '../modelo/PizzeriaDB.php';
$pdo = PizzeriaDB::connectDB();
//Definimos las variables con valores vacíos
$username = $contra = $confirmar_contra = "";
$username_err = $contra_err = $confirmar_contra_err = "";

// Procesamos el formulario cuando le da a enviar
if($_SERVER["REQUEST_METHOD"] == "POST") {

    //Validar al usuario
    if(empty(trim($_POST['username']))) {
        $username_err = "Por favor, introduzca un nombre de usuario";
    }
    else if(!preg_match('/^[a-zA-Z0-9_-]+$/',trim($_POST['username']))){
        $username_err = "El nombre de usuario sólo puede contener letras,
        numeros y guiones bajos.";
    } 
    else {
        $sql = "SELECT id from users WHERE username =:username";

        if($stmt = $pdo->prepare($sql)) {
            // Enlazar variables a la instrucción como parámetros
            $stmt->bindParam(":username",$param_username, PDO::PARAM_STR);
            // Establecemos los parámetros
            $param_username = trim($_POST["username"]);
            // Ejecutar la instrucción
            if($stmt->execute()){
                if($stmt->rowCount() == 1) {
                    $username_err = "Este usuario ya está siendo utilizado";
                }
                else {
                    $username = trim($_POST["username"]);
                } 
                }
                else {
                    echo "Algo muy chungo ha pasado en el camino";
                }
                //Cerramos la instrucción
                unset($stmt);
            }

        }
        // Validamos la contraseña
        if(empty(trim($_POST["contra"]))){
            $contra_err = "Por favor, introduzca una contraseña";
        } elseif (strlen(trim($_POST["contra"])) < 7){
            $contra_err = "La contraseña al menos tiene que tener 7 caracteres";
        } 
        else {
            $contra = trim($_POST["contra"]);
        }

        //Comprobar que las 2 contraseñas son iguales
        if(empty(trim($_POST["confirmar_contra"]))) {
            $confirmar_contra_err = "Por favor, confirme la contraseña";
        } 
        else {
            $confirmar_contra = trim($_POST["confirmar_contra"]);
            if(empty($contra_err) && ($contra != $confirmar_contra)) {
                $confirmar_contra_err = "Las contraseñas no coinciden";
            }
        }
//Comprobamos si hay errores antes de insertar en la base de datos
if (empty($username_err) && empty($contra_err) && empty($confirmar_contra_err)) {
    // Preparamos la insercion en la BBDD.
    $sql = "INSERT INTO users (username,contra) VALUES (:username,:contra)";

    if($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":username", $param_username, PDO::PARAM_STR);
        $stmt->bindParam(":contra", $param_contra, PDO::PARAM_STR);
        // Creamos los parámetros
        $param_username = $username;
        $param_contra = password_hash($contra, PASSWORD_DEFAULT);

        if($stmt->execute()) {
            // Redireccionamos a la página de login
            header("location: login.php");
        }
        else {
            echo "Me he hecho un lío con la puta contraseña";
        }
        unset($stmt);
    }
}
unset($pdo);       
    
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Formulario de Registro</title>
    <style>
        body {
            font: 14px sans-serif;
        }
        .wrapper {
            width: 300px;
            padding: 20px;
        }
    </style>
  </head>
  <body>
      <div class="wrapper">
    <h2>Regístrate</h2>
    <p>Por favor, completa el formulario para crear una cuenta.</p>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
    
    <div class="form-group has-validation">
    <label>Usuario</label>
    <input type="text" name="username" class="form-control" <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?> value="<?php echo $username; ?>">
    <span class="invalid-feedback"><?php echo $username_err; ?></span>
    </div>
        <br>
    <div class="form-group">
    <label>Contraseña</label>
    <input type="password" name="contra" class="form-control" <?php echo (!empty($contra_err)) ? 'is-invalid' : ''; ?> value="<?php echo $contra; ?>">
    <span class="invalid-feedback"><?php echo $contra_err; ?></span>
    </div>
        <br>
    <div class="form-group">
    <label>Confirmar contraseña</label>
    <input type="password" name="confirmar_contra" class="form-control" <?php echo (!empty($confirmar_contra_err)) ? 'is-invalid' : ''; ?> value="<?php echo $confirmar_contra; ?>">
    <span class="invalid-feedback"><?php echo $confirmar_contra_err; ?></span>
    </div>
        <br>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Enviar">
        <input type="reset" class="btn btn-secondary ms-2" value="Borrar">
</div>
        <br>
    <p>¿Tienes una cuenta ya? Pues haz login <a href="login.php">aquí</a>.
</form>
</div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>