<?php
// Inicializamos la sesión
session_start();
// Conexion
require_once '../modelo/PizzeriaDB.php';
$pdo = PizzeriaDB::connectDB();

 

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: bienvenida.php");
    exit;
}
 
require_once "../modelo/PizzeriaDB.php";
 
$username = $password = "";
$username_err = $password_err = $login_err = "";
 
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    if(empty(trim($_POST["username"]))){
        $username_err = "Introduzca el nombre de usuario.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    if(empty(trim($_POST["contra"]))){
        $contra_err = "Por favor, introduzca la contraseña.";
    } else{
        $contra = trim($_POST["contra"]);
    }
    
    if(empty($username_err) && empty($contra_err)){
       
        $sql = "SELECT id, username, password FROM users WHERE username = :username";
        
        if($stmt = $pdo->prepare($sql)){
            
            $stmt->bindParam(":username", $param_username, PDO::PARAM_STR);
            
            $param_username = trim($_POST["username"]);
            
            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    if($row = $stmt->fetch()){
                        $id = $row["id"];
                        $username = $row["username"];
                        $hashed_password = $row["contra"];
                        if(password_verify($password, $hashed_password)){
                            session_start();
                            
                            
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            header("location: bienvenida.php");
                        } else{
                            $login_err = "Usuario o contraseña no válidos.";
                        }
                    }
                } else{
                    
                    $login_err = "Usuario o contraseña no válidos.";
                }
            } else{
                echo "Algo ha ido mal...";
            }

           
            unset($stmt);
        }
    }
    
    unset($pdo);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; }
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Login</h2>
        <p>Rellene sus credenciales para hacer login.</p>

        <?php 
        if(!empty($login_err)){
            echo '<div class="alert alert-danger">' . $login_err . '</div>';
        }        
        ?>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Usuario</label>
                <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Contraseña</label>
                <input type="password" name="contra" class="form-control <?php echo (!empty($contra_err)) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Entrar">
            </div>
            <p>¿No tienes cuenta todavía? <a href="registro.php">Regístrate ahora</a>.</p>
        </form>
    </div>
</body>
</html>
